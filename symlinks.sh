#!/bin/bash
ln -s ~/dotfiles/.bash_aliases ~/
ln -s ~/dotfiles/.bashrc ~/
ln -s ~/dotfiles/.gitconfig ~/
ln -s ~/dotfiles/.vimrc ~/
ln -s ~/dotfiles/.vim/ ~/
